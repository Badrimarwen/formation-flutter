import 'package:calculator/app/modules/calcul_history/bloc/calcul_history_bloc.dart';
import 'package:calculator/app/modules/history/data/models/history_model.dart';
import 'package:calculator/app/modules/recipe/bloc/recipe_bloc.dart';
import 'package:calculator/app/screens/history_detail/history_detail_screen.dart';
import 'package:calculator/core/di/locator.dart';
import 'package:calculator/app_routes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  setupLocator();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({Key? key}) : super(key: key);

  final CalculHistoryBloc calculHistoryBloc = locator<CalculHistoryBloc>();
  final RecipeBloc recipeBloc = locator<RecipeBloc>();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Fomation Flutter ENI Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      builder: (_, widget) {
        return MultiBlocProvider(providers: [
          /// Creating a new instance of the bloc and providing it to the widget tree.
          BlocProvider<CalculHistoryBloc>(create: (_) => calculHistoryBloc),
          BlocProvider<RecipeBloc>(create: (_) => recipeBloc)
        ], child: widget ?? Container());
      },
      initialRoute: splashRoute,
      routes: routes,
      onGenerateRoute: (settings) {
        if (settings.name == historyDetailsRoute) {
          return MaterialPageRoute(
              builder: (context) =>
                  HistoryDetailScreen(history: settings.arguments as History));
        }
      },
    );
  }
}
