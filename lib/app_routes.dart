import 'package:calculator/app/screens/history/history_screen.dart';
import 'package:calculator/app/screens/home/home_screen.dart';
import 'package:calculator/app/screens/login/login_screen.dart';
import 'package:calculator/app/screens/recipe/recipe_screen.dart';
import 'package:calculator/app/screens/splash/splash_screen.dart';
import 'package:flutter/material.dart';

const splashRoute = '/';
const loginRoute = '/login';
const homeRoute = '/home';
const historyRoute = '/history';
const historyDetailsRoute = '/history/details';
const recipeRoute = '/recipe';

const factureRoute = '/facture';
const factureDetailsRoute = '/facture/details';

Map<String, WidgetBuilder> routes = {
  splashRoute: (context) => SplashScreen(),
  loginRoute: (context) => LoginScreen(),
  homeRoute: (context) => HomeScreen(),
  historyRoute: (context) => HistoryScreen(),
  recipeRoute: (context) => RecipeScreen()
};
