import 'package:calculator/app/modules/calcul_history/bloc/calcul_history_bloc.dart';
import 'package:calculator/app/modules/recipe/bloc/recipe_bloc.dart';
import 'package:get_it/get_it.dart';

GetIt locator = GetIt.instance;

void setupLocator() {
  locator.registerLazySingleton(() => CalculHistoryBloc());
  locator.registerLazySingleton(() => RecipeBloc());
}
