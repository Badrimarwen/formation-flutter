List<Month> datas = [
  Month(key: 1, value: "Janvier"),
  Month(key: 2, value: "Fevrier"),
  Month(key: 3, value: "Mars"),
  Month(key: 4, value: "Avril "),
  Month(key: 5, value: "Mai"),
  Month(key: 6, value: "Juin"),
  Month(key: 7, value: "Juillet"),
  Month(key: 8, value: "Août"),
  Month(key: 9, value: "Septembre"),
  Month(key: 10, value: "Octobre"),
  Month(key: 11, value: "Novembre"),
  Month(key: 12, value: "Décembre"),
];

class Month {
  int? key;
  String? value;

  Month({this.key, this.value});
}
