import 'package:calculator/app/screens/login/data/months.dart';
import 'package:flutter/material.dart';

class LoginForm extends StatefulWidget {
  const LoginForm({
    Key? key,
  }) : super(key: key);

  @override
  State<LoginForm> createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _depenseMensController = TextEditingController();
  double dailyEstimation = 0.0;
  int lastDay = 31;
  int month = 0;

  AutovalidateMode autovalidateMode = AutovalidateMode.disabled;
  String? _selectedMonth;
  DateTime now = DateTime.now();

  calculateDaily(BuildContext context) {
    if (_formKey.currentState!.validate()) {
      FocusScope.of(context).unfocus();
      if (month < 12) {
        lastDay = DateTime(now.year, month + 1, 0).day;
      }

      setState(() {
        dailyEstimation = int.parse(_depenseMensController.text) / lastDay;
      });
    } else {
      autovalidateMode = AutovalidateMode.always;
      setState(() {});
    }
  }

  List<DropdownMenuItem<String>> _createMonthsList() {
    return datas
        .map<DropdownMenuItem<String>>(
          (e) => DropdownMenuItem<String>(
            value: e.key.toString(),
            child: Text(e.value!, overflow: TextOverflow.ellipsis),
          ),
        )
        .toList();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      autovalidateMode: autovalidateMode,
      key: _formKey,
      child: Column(
        children: [
          TextFormField(
            controller: _depenseMensController,
            keyboardType: TextInputType.number,
            validator: (value) {
              if (value!.isEmpty) {
                return 'Les dépenses mensuelles ne doivent pas être vide';
              }
              if (int.tryParse(value).toString() == 'null' ||
                  value.length < 3) {
                return 'Les dépenses mensuelles doivent être supérieures au SMIG';
              }
              return null;
            },
          ),
          SizedBox(height: 20),
          DropdownButtonFormField(
              decoration: const InputDecoration(hintText: "Choisissez le mois"),
              onChanged: (String? val) {
                setState(() {
                  _selectedMonth = val!;
                  month = int.parse(_selectedMonth!);
                });
              },
              autovalidateMode: AutovalidateMode.onUserInteraction,
              validator: (value) {
                if (value == null) {
                  return 'Il faut choisir un mois';
                }

                return null;
              },
              value: _selectedMonth,
              items: _createMonthsList()),
          SizedBox(height: 20),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: ElevatedButton(
                onPressed: () {
                  calculateDaily(context);
                },
                child: Text("Calculer")),
          ),
          dailyEstimation > 0
              ? Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                      "DMJ : ${dailyEstimation.toStringAsFixed(2)} EUR/JOUR",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.green,
                          fontWeight: FontWeight.bold,
                          fontSize: 30)),
                )
              : Container(),
        ],
      ),
    );
  }
}
