import 'dart:convert';

import 'package:calculator/app/modules/facture/data/models/facture_model.dart';
import 'package:calculator/app/modules/history/data/models/history_model.dart';
import 'package:flutter/material.dart';

class FactureDetailScreen extends StatelessWidget {
  const FactureDetailScreen({Key? key, required this.data}) : super(key: key);
  final Facture data;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          foregroundColor: Colors.black,
          title: Text("Details de l'opération ${data.reference}"),
        ),
        body: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                data.fournisseur,
                style: TextStyle(fontSize: 40),
              ),
              Text(
                data.dateDeFacturation.toString(),
                style: TextStyle(fontSize: 20),
              ),
              Text(
                data.somme.toString(),
                style: TextStyle(fontSize: 70),
              ),
            ],
          ),
        ));
  }
}
