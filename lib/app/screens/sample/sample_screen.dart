import 'package:calculator/app/screens/sample/vote_model.dart';
import 'package:calculator/app/screens/sample/widgets/card_element.dart';
import 'package:flutter/material.dart';

class SampleScreen extends StatelessWidget {
  SampleScreen({Key? key}) : super(key: key);

  List<Vote> myList = [
    Vote(
        title: "My long title",
        subtitle: " my subtitle my subtitle ",
        votes: 33),
    Vote(
        title: "My long title 2",
        subtitle: " my subtitle my subtitle 2 ",
        votes: 1),
    Vote(
        title: "My long title 3",
        subtitle: " my subtitle my subtitle 3 ",
        votes: 6),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          foregroundColor: Colors.black,
          elevation: 0,
          centerTitle: false,
          title: const Text('Sample'),
          leading: IconButton(
            icon: Icon(Icons.menu),
            onPressed: () {},
          ),
          actions: [Icon(Icons.logout), Icon(Icons.person)],
        ),
        body: /* Stack(children: [
        Container(
          height: 300,
          width: 300,
          color: Colors.red,
        ),
        Align(
          alignment: Alignment.topRight,
          child: Container(
            height: 100,
            width: 200,
            color: Colors.green,
          ),
        ),
        Positioned(
          top: 20,
          left: 20,
          child: Container(
            height: 200,
            width: 100,
            color: Colors.blue,
          ),
        ),
      ]), */
            Row(
          children: [
            Expanded(
              flex: 5,
              child: Container(
                  color: Colors.amber.withOpacity(0.4),
                  child: Text("Test test")),
            ),
            Expanded(
                flex: 1,
                child: Container(
                    color: Colors.red.withOpacity(0.2),
                    child: Text("test test")))
          ],
        ));
  }
}
