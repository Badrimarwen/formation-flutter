import 'package:calculator/app/screens/splash/widgets/list_element.dart';
import 'package:calculator/app_routes.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  loadDelay() {
    Future.delayed(Duration(seconds: 2), () {
      Navigator.pushReplacementNamed(context, homeRoute);
    });
  }

  @override
  void initState() {
    loadDelay();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      body: SafeArea(
        child: Center(
            child: Column(mainAxisSize: MainAxisSize.min, children: [
          Image.asset(
            "assets/images/logo.png",
          ),
          Text(
            "Ma\nCalculatrice",
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 60, fontWeight: FontWeight.bold),
          ),
          Padding(
            padding: EdgeInsets.all(8),
            child: CircularProgressIndicator(),
          )
        ])),
      ),
    );
  }
}
