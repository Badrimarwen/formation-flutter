class ButtonModel {
  final String content;
  final bool isDark;
  final bool doesStratch;

  ButtonModel(
      {required this.content, this.isDark = true, this.doesStratch = false});
}
