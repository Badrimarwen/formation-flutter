import 'package:flutter/material.dart';

class ResultArea extends StatelessWidget {
  const ResultArea({
    Key? key,
    this.equation = '',
    this.result = '',
  }) : super(key: key);

  final equation;
  final result;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(crossAxisAlignment: CrossAxisAlignment.end, children: [
        Text(
          equation,
          style: TextStyle(fontSize: 20),
        ),
        Text(
          result,
          style: TextStyle(fontSize: 70),
        ),
      ]),
    );
  }
}
