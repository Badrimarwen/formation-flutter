import 'package:calculator/app/screens/home/home_screen.dart';
import 'package:calculator/app/screens/home/models/button_model.dart';
import 'package:flutter/material.dart';

class Button extends StatelessWidget {
  Button({
    Key? key,
    required this.model,
    required this.onTapFunction,
  }) : super(key: key);

  final ButtonModel model;
  final Function(String) onTapFunction;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: model.doesStratch ? 2 : 1,
      child: AspectRatio(
        aspectRatio: model.doesStratch ? 2 : 1,
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: InkWell(
            splashColor: Colors.red,
            borderRadius: BorderRadius.circular(30),
            onTap: () {
              onTapFunction(model.content);
            },
            child: Ink(
              decoration: BoxDecoration(
                border: Border.all(color: Colors.black, width: 3),
                borderRadius: BorderRadius.circular(30),
                color: model.isDark ? Colors.black : Colors.white,
              ),
              child: Center(
                child: Text(
                  model.content,
                  style: TextStyle(
                      fontSize: 35,
                      color: model.isDark ? Colors.white : Colors.black),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
