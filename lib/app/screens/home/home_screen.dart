import 'package:calculator/app/modules/calcul_history/bloc/calcul_history_bloc.dart';
import 'package:calculator/app/modules/calcul_history/bloc/calcul_history_event.dart';
import 'package:calculator/app/modules/calcul_history/bloc/calcul_history_state.dart';
import 'package:calculator/app/modules/calcul_history/data/models/last_equation.dart';
import 'package:calculator/app/modules/calcul_history/data/provider/cache/calcul_history_cache_provider.dart';
import 'package:calculator/app/modules/calcul_history/data/repository/calcul_history_repository.dart';
import 'package:calculator/app/modules/history/data/repository/history_repository.dart';
import 'package:calculator/app/screens/home/models/button_model.dart';
import 'package:calculator/app/screens/home/widgets/button.dart';
import 'package:calculator/app_routes.dart';
import 'package:calculator/core/di/locator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:function_tree/function_tree.dart';
import 'widgets/result_area.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  String equation = '';
  String result = '';
  bool isBlackText = true;
  CalculHistoryBloc calculHistoryBloc = locator<CalculHistoryBloc>();
  List<LastEquation> equations = [];

  List<List<ButtonModel>> keyboard = [
    [
      ButtonModel(content: 'C', isDark: false),
      ButtonModel(content: '(', isDark: false),
      ButtonModel(content: ')', isDark: false),
      ButtonModel(content: '/', isDark: false)
    ],
    [
      ButtonModel(content: '7'),
      ButtonModel(
        content: '8',
      ),
      ButtonModel(
        content: '9',
      ),
      ButtonModel(content: 'X', isDark: false)
    ],
    [
      ButtonModel(content: '4'),
      ButtonModel(
        content: '5',
      ),
      ButtonModel(
        content: '6',
      ),
      ButtonModel(content: '-', isDark: false)
    ],
    [
      ButtonModel(content: '1'),
      ButtonModel(
        content: '2',
      ),
      ButtonModel(
        content: '3',
      ),
      ButtonModel(content: '+', isDark: false)
    ],
    [
      ButtonModel(content: '0', doesStratch: true),
      ButtonModel(
        content: '.',
      ),
      ButtonModel(
        content: '=',
      ),
    ],
  ];

  addElementToEquation(String value) {
    setState(() {
      if (value == "C") {
        equation = '';
      } else if (value == "=") {
        evaluateEquation();
        equation = '';
      } else if (value == "X") {
        equation += "*";
      } else {
        equation = equation + value;
      }
    });
  }

  evaluateEquation() {
    calculHistoryBloc.add(SaveCalcul(equation: equation));
  }

  @override
  initState() {
    getLastResult();
    super.initState();
  }

  getLastResult() async {}
  //Exercise: changer la couleur du texte de l'équation selon la couleur du bouton :
  //- si le bouton est noir, le texte doit être noir
  //- si le bouton est blanc, le texte doit être rouge

  changeColor(bool isDark) {}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar:
            AppBar(backgroundColor: Colors.transparent, elevation: 0, actions: [
          Padding(
            padding: EdgeInsets.all(8.0),
            child: IconButton(
              icon: Icon(Icons.save, color: Colors.black),
              onPressed: () {
                Navigator.pushNamed(
                  context,
                  recipeRoute,
                );
              },
            ),
          )
        ]),
        body: BlocListener<CalculHistoryBloc, CalculHistoryState>(
          listener: (context, state) {
            if (state is CalculHistorySavingLoading) {
              print("We are calculating");
            }
            if (state is CalculHistorySavingSuccess) {
              print("We are done calculating");
              print(state.result);
              setState(() {
                result = state.result;
              });
            }
            if (state is CalculHistorySavingError) {
              print("Something went wrong !");
              print(state.error);
            }
            // TODO: implement listener
          },
          child: SafeArea(
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  ResultArea(
                    equation: equation,
                    result: result,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(50.0),
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: keyboard
                            .map((row) => Row(
                                children: row
                                    .map(
                                      (e) => Button(
                                        model: e,
                                        onTapFunction: addElementToEquation,
                                      ),
                                    )
                                    .toList()))
                            .toList()),
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
