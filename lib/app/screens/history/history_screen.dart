import 'dart:convert';

import 'package:calculator/app/modules/history/data/models/history_model.dart';
import 'package:calculator/app_routes.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class HistoryScreen extends StatefulWidget {
  HistoryScreen({Key? key}) : super(key: key);

  @override
  State<HistoryScreen> createState() => _HistoryScreenState();
}

class _HistoryScreenState extends State<HistoryScreen> {
  List<Map<String, dynamic>> historyList = [
    {'title': 'Total Achats', 'date': '2020-03-09', 'amount': 299.01},
    {'title': 'Mes Dépenses', 'date': '2020-03-09', 'amount': 190.91},
    {'title': 'Crédits', 'date': '2020-03-09', 'amount': 991.21},
    {'title': 'Charges Pros', 'date': '2020-03-09', 'amount': 222.21},
    {'title': 'Divers', 'date': '2020-03-09', 'amount': 299.01},
    {'title': 'Divers', 'date': '2020-03-09', 'amount': 232.21},
    {'title': 'Divers', 'date': '2020-03-09', 'amount': 399.01},
    {'title': 'Divers', 'date': '2020-03-09', 'amount': 592.01},
    {'title': 'Titre 1', 'date': '2020-03-09', 'amount': 684.01},
    {'title': 'Titre 1', 'date': '2020-03-09', 'amount': 115.51},
    {'title': 'Titre 1', 'date': '2020-03-09', 'amount': 115.15},
  ];

  List<History> historyModel = [];

  @override
  void initState() {
    historyModel = historyList.map((e) => History.fromJson(e)).toList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          foregroundColor: Colors.black,
        ),
        body: SafeArea(
          child: ListView(
            children: [
              Column(
                  children: historyModel
                      .map((element) => Padding(
                            padding: const EdgeInsets.all(20),
                            child: InkWell(
                              onTap: () {
                                Navigator.pushNamed(
                                    context, historyDetailsRoute,
                                    arguments: element);
                              },
                              child: Card(
                                elevation: 5,
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.stretch,
                                    children: [
                                      Text(
                                        element.title,
                                        style: TextStyle(fontSize: 40),
                                      ),
                                      Text(
                                        '${element.date.day.toString()}-${element.date.month.toString()}-${element.date.year.toString()}',
                                        style: TextStyle(fontSize: 15),
                                      ),
                                      Text(
                                        element.amount.toString(),
                                        style: TextStyle(fontSize: 40),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ))
                      .toList()),
            ],
          ),
        ));
  }
}
