import 'dart:io';

import 'package:calculator/app/modules/recipe/bloc/recipe_bloc.dart';
import 'package:calculator/app/modules/recipe/data/models/scanned_image.dart';
import 'package:calculator/app/modules/recipe/data/repository/scanned_image_repository.dart';
import 'package:calculator/core/di/locator.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RecipeScreen extends StatefulWidget {
  const RecipeScreen({Key? key}) : super(key: key);

  @override
  State<RecipeScreen> createState() => _RecipeScreenState();
}

class _RecipeScreenState extends State<RecipeScreen> {
  ScannedImageRepository scannedImageRepository = ScannedImageRepository();
  RecipeBloc recipeBloc = locator<RecipeBloc>();
  List<ScannedImage> scannedImages = [];
  List<XFile> pickedImages = [];
  XFile? lastPickedImage;

  @override
  initState() {
    //getAllImages();
    super.initState();
  }

  /*etAllImages() async {
    List<ScannedImage> factures = await scannedImageRepository.getAllImages();
    lastPickedImage = XFile(factures[0].path);
    for (var facture in factures) {
      pickedImages.add(XFile(facture.path));
    }
    setState(() {});
  }*/

  lunchCamera() async {
    final ImagePicker _picker = ImagePicker();
    var pickedImage = await _picker.pickImage(source: ImageSource.camera);
    print(pickedImage?.path);

    if (pickedImage != null) {
      ///A chaque fois on enregistre l'image capturée sur la base des données
      lastPickedImage = pickedImage;
      scannedImages
          .add(ScannedImage(path: pickedImage.path, date: DateTime.now()));
      pickedImages.add(pickedImage);
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        foregroundColor: Colors.black,
        title: const Text('Scan Factures'),
      ),
      body: SafeArea(
        child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const SizedBox(
                    height: 20,
                  ),
                  Center(
                      child: ElevatedButton(
                          onPressed: () {
                            lunchCamera();
                          },
                          child: const Text('Capturer une Facture'))),
                  const SizedBox(
                    height: 20,
                  ),
                  const Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Text(
                      'Ma dernière facture',
                      textAlign: TextAlign.left,
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 30),
                    ),
                  ),
                  if (lastPickedImage != null)
                    Image.file(File(lastPickedImage!.path),
                        width: 400, height: 200),
                  const SizedBox(
                    height: 20,
                  ),
                  const Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Text(
                      'Mes Factures',
                      textAlign: TextAlign.left,
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 30),
                    ),
                  ),
                  //Lire les images de la base de données
                  pickedImages.isEmpty
                      ? Container()
                      : GridView.count(
                          shrinkWrap: true,
                          primary: false,
                          padding: const EdgeInsets.all(10),
                          crossAxisSpacing: 2,
                          mainAxisSpacing: 2,
                          crossAxisCount: 3,
                          children: pickedImages
                              .map(
                                (pickedImage) => Padding(
                                  padding: const EdgeInsets.all(2.0),
                                  child: Image.file(File(pickedImage.path)),
                                ),
                              )
                              .toList()),
                ],
              ),
            )),
      ),
    );
  }
}
