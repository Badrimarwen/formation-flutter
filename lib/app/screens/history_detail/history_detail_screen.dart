import 'dart:convert';

import 'package:calculator/app/modules/history/data/models/history_model.dart';
import 'package:flutter/material.dart';

class HistoryDetailScreen extends StatelessWidget {
  const HistoryDetailScreen({Key? key, required this.history})
      : super(key: key);
  final History history;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          foregroundColor: Colors.black,
          title: Text("Details de l'opération ${history.title}"),
        ),
        body: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                history.title,
                style: TextStyle(fontSize: 40),
              ),
              Text(
                history.date.toString(),
                style: TextStyle(fontSize: 20),
              ),
              Text(
                history.amount.toString(),
                style: TextStyle(fontSize: 70),
              ),
            ],
          ),
        ));
  }
}
