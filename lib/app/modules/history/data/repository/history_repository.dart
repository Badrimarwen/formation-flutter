import 'package:calculator/app/modules/history/data/providers/cache/history_cache_provider.dart';

class HistoryRepository {
  final HistoryCacheProvider _historyCacheProvider = HistoryCacheProvider();

  saveCalculatorLastResultToCache(String result) {
    _historyCacheProvider.saveResult(result);
  }

  Future<String?> getCalculatorResultFromCache() {
    return _historyCacheProvider.getResult();
  }
}
