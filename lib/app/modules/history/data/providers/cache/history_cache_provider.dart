import 'package:flutter/foundation.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HistoryCacheProvider {
//Methode pour sauvegarder le dernier resultat de la calcul
  saveResult(String result) async {
    try {
      final prefs = await SharedPreferences.getInstance();
      await prefs.setString('last_result', result);
    } catch (e) {
      print(e);
    }
  }

  //methode pour lire le dernier resultat
  Future<String?> getResult() async {
    try {
      final prefs = await SharedPreferences.getInstance();
      return prefs.getString('last_result') ?? '';
    } catch (e) {
      print(e);
    }
  }
}
