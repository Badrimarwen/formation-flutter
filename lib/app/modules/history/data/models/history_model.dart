class History {
  History(
      {this.id, required this.title, required this.date, required this.amount});
  final int? id;
  final String title;
  final DateTime date;
  final double amount;

  factory History.fromJson(Map<String, dynamic> json) {
    int? id = json['id'];
    String title = json['title'];
    DateTime date = DateTime.parse(json['date']);
    double amount = json['amount'];
    return History(id: id, title: title, date: date, amount: amount);
  }
}
