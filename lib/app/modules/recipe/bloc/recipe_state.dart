import 'package:equatable/equatable.dart';
import 'package:image_picker/image_picker.dart';

abstract class RecipeState extends Equatable {
  const RecipeState();
}

class RecipeInitial extends RecipeState {
  @override
  List<Object> get props => [];
}

class NewRecipeSavingLoading extends RecipeState {
  @override
  List<Object> get props => [];
}

class NewRecipeSavingSuccess extends RecipeState {
  @override
  List<Object> get props => [];
}

class NewRecipeSavingError extends RecipeState {
  NewRecipeSavingError({required this.error});
  final String error;

  @override
  List<Object> get props => [];
}

class GetAllImagesLoading extends RecipeState {
  @override
  List<Object> get props => [];
}

class GetAllImagesSuccess extends RecipeState {
  GetAllImagesSuccess({required this.scannedImages});
  final List<XFile> scannedImages;
  @override
  List<Object> get props => [];
}

class GetAllImagesError extends RecipeState {
  GetAllImagesError({required this.error});
  final String error;

  @override
  List<Object> get props => [];
}
