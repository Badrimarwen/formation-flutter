import 'package:equatable/equatable.dart';

abstract class RecipeEvent extends Equatable {
  const RecipeEvent();
}

class SaveNewRecipe extends RecipeEvent {
  @override
  List<Object?> get props => [];
}

class GetAllImages extends RecipeEvent {
  @override
  List<Object?> get props => [];
}
