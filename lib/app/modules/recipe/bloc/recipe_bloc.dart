import 'package:calculator/app/modules/recipe/bloc/recipe_event.dart';
import 'package:calculator/app/modules/recipe/bloc/recipe_state.dart';
import 'package:calculator/app/modules/recipe/data/models/scanned_image.dart';
import 'package:calculator/app/modules/recipe/data/repository/scanned_image_repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';

class RecipeBloc extends Bloc<RecipeEvent, RecipeState> {
  RecipeBloc() : super(RecipeInitial()) {
    on<SaveNewRecipe>(_onSaveNewRecipe);
    on<GetAllImages>(_onGetAllImages);
  }
  final ScannedImageRepository _scannedImageRepository =
      ScannedImageRepository();
  _onSaveNewRecipe(SaveNewRecipe event, Emitter<RecipeState> emit) async {
    emit(NewRecipeSavingLoading());
    try {
      // Sauvegarder une image dans la base des données Locale
      emit(NewRecipeSavingSuccess());
    } catch (e) {
      emit(NewRecipeSavingError(error: e.toString()));
    }
  }

  _onGetAllImages(GetAllImages event, Emitter<RecipeState> emit) async {
    emit(GetAllImagesLoading());
    try {
      // Get All Images from local Database
      List<XFile> pickedImages = [];
      XFile? lastPickedImage;
      List<ScannedImage> factures =
          await _scannedImageRepository.getAllImages();
      lastPickedImage = XFile(factures[0].path);
      for (var facture in factures) {
        pickedImages.add(XFile(facture.path));
      }
      emit(GetAllImagesSuccess(scannedImages: pickedImages));
    } catch (e) {
      emit(GetAllImagesError(error: e.toString()));
    }
  }
}
