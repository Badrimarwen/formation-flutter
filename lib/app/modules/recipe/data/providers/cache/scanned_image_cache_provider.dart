import 'package:calculator/app/modules/recipe/data/models/scanned_image.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite/sqlite_api.dart';

class ScannedImageCacheProvider {
  Database? db;

  Future initialiseDatabse() async {
    var _databasePath = await getDatabasesPath();

    db = await openDatabase(join(_databasePath, 'calculator.db'), version: 1,
        onCreate: (Database db, int version) async {
      await db.execute('''
  create table images ( 
  id integer primary key autoincrement, 
  path text not null,
  date integer not null)
''');
    });
  }

  Future<ScannedImage> insert(ScannedImage facture) async {
    await initialiseDatabse();
    var newId = await db!.insert('images', facture.toJson());
    print(newId);
    return ScannedImage(id: newId, path: facture.path, date: facture.date);
  }

  Future<List<ScannedImage>> getAll() async {
    await initialiseDatabse();
    var result = await db!.query('images');
    return result.map((e) => ScannedImage.fromJson(e)).toList();
  }
}
