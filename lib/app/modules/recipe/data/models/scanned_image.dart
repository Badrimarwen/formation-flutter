class ScannedImage {
  ScannedImage({this.id, required this.path, required this.date});
  final int? id;
  final String path;
  final DateTime date;

  factory ScannedImage.fromJson(Map<String, dynamic> json) {
    int? id = json['id'];
    String path = json['path'];
    DateTime date = DateTime.parse(json['date']);
    return ScannedImage(id: id, path: path, date: date);
  }
  Map<String, dynamic> toJson() {
    return {'id': id, 'path': path, 'date': 0};
  }
}
