import 'package:calculator/app/modules/recipe/data/models/scanned_image.dart';
import 'package:calculator/app/modules/recipe/data/providers/cache/scanned_image_cache_provider.dart';

class ScannedImageRepository {
  ScannedImageCacheProvider _scannedImageCacheProvider =
      ScannedImageCacheProvider();

  // Faire appel aux fonctions définies à ScannedImageCacheProvider

  addImage(ScannedImage facture) async {
    await _scannedImageCacheProvider.insert(facture);
  }

  Future<List<ScannedImage>> getAllImages() async {
    return await _scannedImageCacheProvider.getAll();
  }
}
