class LastEquation {
  LastEquation({this.id, required this.equation, required this.result});
  final int? id;
  final String equation;
  final String result;

  factory LastEquation.fromJson(Map<String, dynamic> json) {
    int? id = json['id'];
    String equation = json['equation'];
    String result = json['result'];

    return LastEquation(id: id, equation: equation, result: result);
  }
  Map<String, dynamic> toJson() {
    return {'id': id, 'equation': equation, 'result': result};
  }
}
