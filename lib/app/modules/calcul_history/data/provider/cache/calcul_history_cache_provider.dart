import 'package:calculator/app/modules/calcul_history/data/models/last_equation.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class CalCulHistoryCacheProvider {
  Database? db;
  Future initDatabase() async {
    var databasePath = await getDatabasesPath();
    db = await openDatabase(join(databasePath, 'calculator.db'), version: 1,
        onCreate: (Database db, int version) async {
      await db.execute('''
  create table calculs ( 
  id integer primary key autoincrement, 
  equation text not null,
  result text not null)
''');
    });
  }

  Future<LastEquation> insert(LastEquation lastEquation) async {
    await initDatabase();
    var newId = await db!.insert('calculs', lastEquation.toJson());
    return LastEquation(
        id: newId,
        equation: lastEquation.equation,
        result: lastEquation.result);
  }

  Future<List<LastEquation>> getAll() async {
    await initDatabase();
    var calculs = await db!.query('calculs');
    return calculs.map((e) => LastEquation.fromJson(e)).toList();
  }
}
