import 'package:calculator/app/modules/calcul_history/data/models/last_equation.dart';
import 'package:calculator/app/modules/calcul_history/data/provider/cache/calcul_history_cache_provider.dart';

class CalculHistoryRepository {
  final CalCulHistoryCacheProvider _historyCacheProvider =
      CalCulHistoryCacheProvider();

  Future<LastEquation> insertLastEquation(LastEquation equation) async {
    var result = await _historyCacheProvider.insert(equation);
    return result;
  }

  Future<List<LastEquation>> getAllEquations() async {
    var result = await _historyCacheProvider.getAll();
    return result;
  }
}
