import 'package:equatable/equatable.dart';

abstract class CalculHistoryState extends Equatable {
  const CalculHistoryState();
}

class CalculHistoryInitial extends CalculHistoryState {
  @override
  List<Object> get props => [];
}

class CalculHistorySavingLoading extends CalculHistoryState {
  @override
  List<Object> get props => [];
}

class CalculHistorySavingSuccess extends CalculHistoryState {
  CalculHistorySavingSuccess({required this.result});
  final String result;

  @override
  List<Object> get props => [];
}

class CalculHistorySavingError extends CalculHistoryState {
  CalculHistorySavingError({required this.error});
  final String error;

  @override
  List<Object> get props => [];
}
