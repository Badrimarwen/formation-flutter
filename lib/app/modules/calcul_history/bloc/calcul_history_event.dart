import 'package:equatable/equatable.dart';

abstract class CalculHistoryEvent extends Equatable {
  const CalculHistoryEvent();
}

class SaveCalcul extends CalculHistoryEvent {
  const SaveCalcul({required this.equation});
  final String equation;

  @override
  List<Object?> get props => [];
}
