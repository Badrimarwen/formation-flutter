import 'package:calculator/app/modules/calcul_history/bloc/calcul_history_event.dart';
import 'package:calculator/app/modules/calcul_history/bloc/calcul_history_state.dart';
import 'package:calculator/app/modules/calcul_history/data/models/last_equation.dart';
import 'package:calculator/app/modules/calcul_history/data/repository/calcul_history_repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:function_tree/function_tree.dart';

class CalculHistoryBloc extends Bloc<CalculHistoryEvent, CalculHistoryState> {
  CalculHistoryBloc() : super(CalculHistoryInitial()) {
    on<SaveCalcul>(_onSaveCalcul);
  }
  final CalculHistoryRepository _calculHistoryRepository =
      CalculHistoryRepository();
  _onSaveCalcul(SaveCalcul event, Emitter<CalculHistoryState> emit) async {
    emit(CalculHistorySavingLoading());
    try {
      String? result;
      print("Im here trying to calculate this equation");
      print(event.equation);

      result = event.equation.interpret().toString();

      var index = await _calculHistoryRepository.insertLastEquation(
          LastEquation(equation: event.equation, result: result));
      emit(CalculHistorySavingSuccess(result: result));
    } catch (e) {
      emit(CalculHistorySavingError(error: e.toString()));
    }
  }
}
