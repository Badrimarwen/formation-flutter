class Facture {
  Facture(
      {this.reference,
      required this.fournisseur,
      required this.dateDeFacturation,
      required this.somme});
  final int? reference;
  final String fournisseur;
  final DateTime dateDeFacturation;
  final double somme;

  factory Facture.fromJson(Map<String, dynamic> json) {
    int? reference = json['reference'];
    String fournisseur = json['fournisseur'];
    DateTime dateDeFacturation = DateTime.parse(json['dateDeFacturation']);
    double somme = json['somme'];
    return Facture(
        reference: reference,
        fournisseur: fournisseur,
        dateDeFacturation: dateDeFacturation,
        somme: somme);
  }
}
